#!/usr/bin/env python3
# coding: utf-8
#
# 応答生成モジュール
# 基本的には
# - 入力と応答の対応リスト(argv[1])
# - 話者認識結果ID (argv[2])
# - 音声認識結果 (argv[3])
# を受け取って応答文および音声を生成する
#
# 前の応答への依存性を持たせたい場合は引数を追加すれば良い
import sys, os
import urllib.request, json, datetime, pytz, re, random
# sudo apt-get install python3-tz
from collections import Counter

# 音声合成エンジンのpath
jtalkbin = 'open_jtalk '
options = '-m /usr/share/hts-voice/nitech-jp-atr503-m001/nitech_jp_atr503_m001.htsvoice -ow /tmp/dialogue/out.wav -x /var/lib/mecab/dic/open-jtalk/naist-jdic'

# 音声合成のコマンドを生成 (open jtalk を 使う場合
def mk_jtalk_command(answer):
    jtalk = 'echo "' + answer + '" | ' + jtalkbin + options + ';'
    play = 'play -q /tmp/dialogue/out.wav; rm /tmp/dialogue/out.wav;'
    return jtalk + play

def get_weather(code):
    """
    天気IDから日本語の天気に変換する
    """
    if code < 300:
        return "大雨"
    elif code < 400:
        return "霧雨"
    elif code < 600:
        return "雨"
    elif code < 700:
        return "雪"
    elif code < 800:
        return "空気が悪い"
    elif code < 900:
        return "晴れ"
    else:
        return "曇り"

def list_matome(li):
    """
    与えられたlistオブジェクトに対し、連続する同じitemを重複排除したlistオブジェクトを返す
    """
    new_li = list()
    for item in li:
        if len(new_li) == 0 or new_li[-1] != item:
            new_li.append(item)
    return new_li

def make_okimochi(info):
    """
    「お気持ち」文字列を生成する
    """
    if '雪' in info['天気']: # 天気に「雪」が含まれる
        return random.choice(["雪で滑らないように気をつけましょう。", "雪だるまつくーろー。"])
    elif '雨' in info['天気']: # 天気に「雨」が含まれる
        return random.choice(["外出時は傘を持ち歩きましょう。", "空が泣いています。遠くであの子も泣いているのかもしれませんね。"])
    elif info['風速'] > 10:
        return random.choice(["強風に気をつけましょう。", "飛ばされちゃわないように手を繋いで歩きましょう。"])
    elif ('最高気温' in info and info['最高気温'] > 20) or ('気温' in info and info['気温'] > 20):
        return random.choice(["過ごしやすくなるでしょう。", "暑ーい。"])
    elif ('最低気温' in info and info['最低気温'] < 5) or ('気温' in info and info['気温'] < 5):
        return random.choice(["防寒対策が必要です。", "寒ーい。"])
    elif ('最高湿度' in info and info['最高湿度'] > 80) or ('湿度' in info and info['湿度'] > 80):
        return random.choice(["ジメジメした一日になりそうです。", "腐るー。"])
    elif ('最低湿度' in info and info['最低湿度'] < 30) or ('湿度' in info and info['湿度'] < 30):
        return random.choice(["カラッとした一日になりそうです。", "ミイラになっちゃう。"])
    else: # 何の条件にも当てはまらなかった場合、以下の適当な「お気持ち」を述べる
        return random.choice([
            "憂鬱な一日になりそうです。", "憂鬱でしょう。",
            "しんど EEIC", "ビールが飲みたくなりますね。",
            "今日はアインクラッドで最高の気象設定だ。こんな日に迷宮に潜ってちゃ勿体ない。",
            "しんどい季節を知ってこその笑顔、極上、スマイル。",
            "たくお前らはいいよな。何でもなく過ごしたり、自分の好きなことができて。俺はお前らがそんなことしてる間に、地球と戦わなきゃいけねえのによ。さーて、いっちょ天気、変えちゃいますか。"
        ])

def make_message(data, info):
    """
    応答を生成する
    """
    result = ""
    if data['対象'] != 'お気持ち': # 「お気持ち」のみの場合は日時・場所を言わない
        result += data['日にち'] + "の"
        if data['時刻'] is not None: # 時刻は指定されている時だけ出す
            result += data['時刻'] + "の"
        result += data['場所'] + "の"
    if data['対象'] == '天気': # 天気を出す
        result += "天気は" + info['天気'] + "です。"
    elif data['対象'] == '気温': # 気温を出す
        if data['時刻'] is None: # 時刻指定がない場合は1日の最高・最低気温を出す
            result += "最高気温は" + str(info['最高気温']).replace('-', "マイナス") + "度、"
            result += "最低気温は" + str(info['最低気温']).replace('-', "マイナス") + "度です。"
        else:
            result += "気温は" + str(info['気温']).replace('-', "マイナス")  + "度です。"
    elif data['対象'] == '湿度': # 湿度を出す
        if data['時刻'] is None:
            result += "最高湿度は" + str(info['最高湿度']) + "パーセント、"
            result += "最低湿度は" + str(info['最低湿度']) + "パーセントです。"
        else:
            result += "湿度は" + str(info['湿度']) + "パーセントです。"
    result += make_okimochi(info) # 最後に必ずお気持ちを付け加える
    return result

def get_weather_info(data):
    """
    天気APIから情報を取得し、加工する
    """
    appid = os.environ['WEATHER_APPID']
    today = datetime.datetime.now(tz = pytz.timezone('Asia/Tokyo')).date()
    日にち一覧 = {'今': today, '今日': today, '明日': today + datetime.timedelta(days=1), '明後日': today + datetime.timedelta(days=2)}
    時刻一覧 = {'朝': '09', '昼': '15', '夜': '21'}
    場所一覧 = {'東京' : '1850147', '大阪': '1853909', '北海道': '2130404', '札幌': '2130404'}
    # （日にちが「今なら」）現況
    if data['日にち'] == '今':
        with urllib.request.urlopen('https://api.openweathermap.org/data/2.5/weather?units=metric&id=' + 場所一覧[data['場所']] + '&appid=' + appid) as fp:
            text = fp.read().decode('utf-8') # APIサーバからのレスポンスを読み取る
            weather = json.loads(text) # JSON文字列をパースする
            target = {
                '天気': get_weather(weather['weather'][0]['id']),
                '気温': weather['main']['temp'], #単位はC
                '湿度': weather['main']['humidity'], #単位は%
                '風速': weather['wind']['speed'] #単位はm/s
            }
    # （それ以外は）予報
    else:
        with urllib.request.urlopen('https://api.openweathermap.org/data/2.5/forecast?units=metric&id=' + 場所一覧[data['場所']] + '&appid=' + appid) as fp:
            text = fp.read().decode('utf-8')
            obj = json.loads(text)
            天気一覧 = list()
            気温一覧 = list()
            湿度一覧 = list()
            風速一覧 = list()
            target = {} # 返答に用いる気象データ
            for weather in obj['list']:
                time = pytz.timezone('UTC').localize(datetime.datetime.utcfromtimestamp(weather['dt'])).astimezone(pytz.timezone('Asia/Tokyo'))
                if time.date() == 日にち一覧[data['日にち']]:
                    if (data['時刻'] is not None) and (time.strftime("%H") == 時刻一覧[data['時刻']]):
                        target = {
                            '天気': get_weather(weather['weather'][0]['id']),
                            '気温': weather['main']['temp'], #単位はC
                            '湿度': weather['main']['humidity'], #単位は%
                            '風速': weather['wind']['speed'] #単位はm/s
                        }
                    天気一覧.append(get_weather(weather['weather'][0]['id']))
                    気温一覧.append(float(weather['main']['temp']))
                    湿度一覧.append(float(weather['main']['humidity']))
                    風速一覧.append(float(weather['wind']['speed']))
            if data['時刻'] is None:
                天気連続まとめ = list_matome(天気一覧)
                天気多い順 = [tuple1[0] for tuple1 in Counter(天気一覧).most_common(2)]
                if len(天気連続まとめ) <= 2:
                    天気まとめ = 'のち'.join(天気連続まとめ) # AのちB
                else:
                    天気まとめ = '時々'.join(天気多い順) # A時々B
                target = {
                    '天気': 天気まとめ,
                    '最低気温': min(気温一覧), '最高気温': max(気温一覧),
                    '最低湿度': min(湿度一覧), '最高湿度': max(湿度一覧),
                    '風速': sum(風速一覧) / len(風速一覧)
                }
    return target

def parse_input(string):
    """
    認識文字列を正規表現で分解する
    """
    # 場所
    場所match = re.search("(東京|大阪|北海道|札幌)の", string)
    if 場所match == None:
        with open("place.txt", "r") as fp: # 入力文字列中にマッチしなかったら、記憶ファイルから読み出す
            場所 = fp.read()
    else:
        場所 = 場所match.group(1)
    with open("place.txt", "w") as fp: # 入力文字列中にマッチしたかに関わらず、書いておこうとする。
        fp.write(場所)

    # 日にち
    日にちmatch = re.search("(今|今日|明日|明後日)の", string)
    if 日にちmatch == None:
        with open("date.txt", "r") as fp:
            日にち = fp.read()
    else:
        日にち = 日にちmatch.group(1)
    with open("date.txt", "w") as fp:
        fp.write(日にち)

    # 時刻
    時刻match = re.search("(朝|昼|夜)の", string)
    if 時刻match == None:
        時刻 = None # 時刻は未指定を許す
    else:
        時刻 = 時刻match.group(1)

    # 対象
    対象match = re.search("(天気|気温|湿度|お気持ち)は？\Z", string)
    対象 = 対象match.group(1) # 対象は未指定を許さない

    return {"場所": 場所, "日にち": 日にち, "時刻": 時刻, "対象": 対象}
    

if __name__ == '__main__':
    data = parse_input(sys.argv[1])
    print("音声認識", data)
    info = get_weather_info(data)
    print("取得結果", info)
    result = make_message(data, info)
    print("応答", result)
    os.system(mk_jtalk_command(result))
